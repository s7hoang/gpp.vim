" Vim indent file
" Language: GPP with html-style macros
" Maintainer: Sunny Hoang
" Latest Revision: 2017-12-26

if exists("GppIndent")
  finish
endif

setlocal indentexpr=GppIndent()

function! GppIndent()
  let line = getline(v:lnum)
  let previousNum = prevnonblank(v:lnum - 1)
  let previousLine = getline(previousNum)
  let angleBracketDifference = AngleBracketDifference(previousLine)

  " align `#endif` with `#ifdef`
  if line =~ '#endif'
    let skip = 0
    let lnum = line('.')
    let lnum = prevnonblank(lnum - 1)
    while lnum > 1
      if getline(lnum) =~ '#endif'
        let skip += 1
      elseif getline(lnum) =~ '#ifdef' && skip > 0
        let skip -= 1
      elseif getline(lnum) =~ '#ifdef' && skip == '0'
        break
      endif
      let lnum = prevnonblank(lnum - 1)
    endwhile
    return indent(lnum)
  endif

  " align `>` with `<` for lines where '>' is the only character on the line
  if line =~ '^\s*>\s*$' " closing angle bracket on its own
    let skip = 0
    let lnum = line('.')
    let lnum = prevnonblank(lnum - 1)
    while lnum > 1
      let difference = AngleBracketDifference(getline(lnum))
      if difference < 0 " more '>'
        let skip += abs(difference)
      elseif difference > 0 && skip > 0 " more '<'
        let skip -= abs(difference)
        if skip < 0
          return indent(lnum) + (shiftwidth() * (abs(skip) - 1))
        endif
      elseif difference > 0 && skip == 0 " more'<'
        return indent(lnum) + (shiftwidth() * (difference - 1))
      endif
      let lnum = prevnonblank(lnum - 1)
    endwhile
    return indent(lnum)
  endif

  if angleBracketDifference > 0
    " '<' without closing '>'
    return indent(previousNum) + (shiftwidth() * angleBracketDifference)
    " closing '>' not on (a) separate line(s)
  elseif angleBracketDifference < 0 && previousLine !~ '^\s*>\s*$' " closing angle bracket on its own
    return indent(previousNum) - (shiftwidth() * abs(angleBracketDifference))
  endif

  return indent(previousNum)

endfunction

function! Occurences(line, char)
  " returns the occurences on 'char' on 'line'
  return len(split(a:line, a:char, 1)) - 1
endfunction

function! AngleBracketDifference(line)
  " + number indicates more '<'
  " - number indicates more '>'
  " 0 indicates same '<' and '>'
  let openAngle = Occurences(a:line, '<')
  let closeAngle = Occurences(a:line, '>')
  return openAngle - closeAngle
endfunction
