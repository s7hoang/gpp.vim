" Vim file detect file
" Language: GPP with html-style macros
" Maintainer: Sunny Hoang
" Latest Revision: January 25, 2017

autocmd BufRead,BufNewFile *.gpp setfiletype gpp | set filetype=gpp

