" Vim syntax file
" Language: GPP with html-style macros
" Maintainer: Sunny Hoang
" Latest Revision: 2017-12-26

syn region gppMacro start='<' end='>' contains=gppMacro,gppMacroStart,gppNextArgument,normal,gppParameter nextgroup=Argument
hi gppMacro ctermfg=Red

syn match gppMacroStart /#/ nextgroup=MetaDirective,MetaDefineDirectiveInactive,MetaDefineDirectiveActive,MacroName contained
hi gppMacroStart ctermfg=Cyan

syn keyword MetaDirective  endif ifneq ifeq include MetaDefineDirectiveInactive MetaDefineDirectiveActive contained
hi MetaDirective ctermfg=Cyan

syn keyword MetaDefineDirectiveInactive ifdef ifndef nextgroup=DefineDirectiveNameInactive skipwhite contained
hi MetaDefineDirectiveInactive ctermfg=Cyan
syn keyword MetaDefineDirectiveActive define nextgroup=DefineDirectiveNameActive,DefineMacroNameActive skipwhite contained
hi MetaDefineDirectiveActive ctermfg=Cyan

syn match DefineDirectiveNameInactive /\<[A-Z][A-Z_0-9]*\>/ contained
hi DefineDirectiveNameInactive ctermfg=grey
syn match DefineDirectiveNameActive /\<[A-Z][A-Z_0-9]*\>/ contained
hi DefineDirectiveNameActive ctermfg=Red cterm=bold

syn match DefineMacroNameActive /\<[a-z][A-Za-z_0-9.]*\>/ contained
hi DefineMacroNameActive ctermfg=Red

syn match MacroName /\<\w\+\>/ contained nextgroup=Argument skipwhite
hi MacroName ctermfg=Cyan

syn match Argument /[^<>]\+/ contained contains=normal,gppNextArgument,gppParameter

syn match gppNextArgument /|/ contained nextgroup=Argument skipwhite
hi gppNextArgument ctermfg=Blue cterm=bold

syn match normal '\\|' contained
syn match normal '\\\\#' contained
syn match normal /[,.:+()@/[\]]/ contained
syn match normal /\w/ contained
syn match normal /\s/ contained

syn match gppParameter /#[0-9]/ contained
hi gppParameter ctermfg=Green cterm=bold

setlocal matchpairs+=<:>
